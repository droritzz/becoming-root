#!/bin/env bash
################################
#owner:droritzz
#purpose: creating partitions with bash script
#date: 18/10/20
#version: in git commit
##############################

diskname=""
read -p "please provide disk name where you want to create partitions " diskname
sudo fdisk /dev/$diskname << EOF
n
p


+1G
EOF


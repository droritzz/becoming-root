#!/usr/bin/env bash
##################################
#owner: droritzz
#purpose: backing up partitions
#date: 21.10.20
#version: in git commit
##################################



#############functions######################
backup_MBR(){
dd if=$disk_name of=$backup_location/MBR.img count=2047 bs=1
}

backup_home_part(){
local home_part=$(lsblk | grep "/home" | awk '{print$1}')
dd if=/dev/$home_part of=$backup_location/home_part.img
}


backup_root_home_part(){
sudo
local root_home_part=$(lsblk | grep "/home" | awk '{print$1}')
dd if=/dev/$root_home_part of=$backup_location/root_home_part.img
}
#func clean


exitstatus(){
if [[ echo $? -gt 0 ]]; then
	echo "cannot create backup"
	exit 1
else
	echo "done"
fi
}

validation(){
	if [[ -z $@ ]]; then
		echo "please provide data"
		exit 1
	fi
}

##############creating backup directory####################
read -p "this is backup creator. please provide $backup_location " backup_location

validation

##############chose menu#########################
read -p "which disk to you want to backup? " disk_name
printf  \n "please select action "
select option in BackUp_MBR BackUp_Home_Partition BackUp_and_zip_Home_Partition BackUp_and_zip_Root_Home_Partition BackUp_and_zip_Whole_Disk Clean_Swap_Partition
do
	        case $option in
                "BackUp_MBR")
                backup_MBR
		exitstatus
                ;;
        "BackUp_Home_Partition")
                backup_home_par
		exitstatus
                ;;
        "BackUp_and_zip_Home_Partition")
                backup_home_par
		gzip -k 
                 ;;
         "BackUp_and_zip_Root_Home_Partition")
                 
                 ;;
         "BackUp_and_zip_Whole_Disk")
                
                ;;
        "Clean_Swap_Partition")
                echo f
                ;;

        esac
        break
done


